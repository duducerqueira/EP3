class CharactersController < ApplicationController

  def index
    @characters = Character.order(:name)
  end

  def new
    @character = Character.new
  end

  def search
    @search_term = params[:term]
    @search_result = Character.where "name like ? or character_class like ? or description like ?", "%#{@search_term}%", "%#{@search_term}%", "%#{@search_term}%"
  end

  def create
    values = params.require(:character).permit :name, :character_class, :description, :physical, :social_skills, :magical_abilities
    @character = Character.new values
    if @character.save
      flash[:notice] = "Character saved successfully"
      redirect_to root_url
    else
      render :new
    end
  end

  def destroy
    id = params[:id]
    Character.destroy id
    redirect_to root_url
  end

end
