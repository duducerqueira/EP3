class Character < ActiveRecord::Base
  validates :name, presence: true
  validates :character_class, presence: true
  validates :physical, presence: true
  validates :social_skills, presence: true
  validates :magical_abilities, presence: true
end
