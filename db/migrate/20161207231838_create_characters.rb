class CreateCharacters < ActiveRecord::Migration
  def change
    create_table :characters do |t|
      t.string :name
      t.string :character_class
      t.text :description
      t.string :physical
      t.string :social_skills
      t.string :magical_abilities

      t.timestamps null: false
    end
  end
end
