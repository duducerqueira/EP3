Rails.application.routes.draw do
  root "characters#index"
  resources :characters, only: [:new, :create, :destroy]
  get "/characters/search" => "characters#search", as: :search_character
end
